# Generated by Django 3.0.5 on 2020-05-17 05:32

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('DOCEOAPP', '0009_add_course_status'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='add_course',
            name='status',
        ),
    ]
