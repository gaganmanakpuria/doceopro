# Generated by Django 3.0.5 on 2020-05-05 21:29

from django.conf import settings
from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
        ('DOCEOAPP', '0004_auto_20200505_2134'),
    ]

    operations = [
        migrations.CreateModel(
            name='add_course',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('course_name', models.CharField(max_length=250)),
                ('course_price', models.CharField(max_length=250)),
                ('Sale_price', models.CharField(max_length=250)),
                ('course_image', models.ImageField(upload_to='courses/%Y/%m/%d')),
                ('details', models.TextField()),
                ('Teacher', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to=settings.AUTH_USER_MODEL)),
                ('course_catagory', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, to='DOCEOAPP.courses_catagory')),
            ],
        ),
    ]
