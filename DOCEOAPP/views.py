from django.shortcuts import render,redirect,reverse
from django.http import HttpResponse,HttpResponseRedirect,JsonResponse
from DOCEOAPP.models import contactmodel,courses_catagory,register_user,add_course,add_video,cart,feedback
from django.contrib.auth.models import User 
from django.contrib.auth import login,authenticate,logout
from django.contrib.auth.decorators import login_required
from DOCEOAPP.forms import add_course_form
from django.shortcuts import get_object_or_404
from django.core.mail import EmailMessage
from django.db.models import Q
import os
from django.conf import settings
from decimal import Decimal
from paypal.standard.forms import PayPalPaymentsForm


def Home(request):
     
    context={}
    
    context['a'] ="line"
    context['active_home'] = "active"
    feedback = contactmodel.objects.all().order_by("firstname")[:4]
    all = add_course.objects.all().order_by("-id")[:4]
    context['feedback'] = feedback
    usr= User.objects.all().filter(is_staff=True)[:3][::-1]
    
    context['courses'] =all
    context['usr'] =usr
    img=[]
    for i in usr:
        img.append(register_user.objects.filter(user=i))
    context['img'] = img
    

    if request.method=="POST":
        if "rsbmt" in request.POST:
            fname = request.POST["fname"]
            lname = request.POST["lname"]
            username = request.POST["uname"]
            email = request.POST["email"]
            contact = request.POST["num"]
            user_type = request.POST["utype"]
            password = request.POST["psd"]
            repeat_password = request.POST["rpsd"]

            usr = User.objects.create_user(username,email,password)
            usr.first_name = fname
            usr.last_name = lname
            if user_type =="Teacher":
                usr.is_staff =True

            usr.save()
            reg = register_user(user=usr,contact_number=contact)
            reg.save()
            print("hello")
            context["status"]="Mr/Miss.{} account created successfully".format(fname)

    
    return render(request,"home.html",context)

def about(request):
    context={}
    context['a'] ="line"
    context['active_about'] = "active"
    return render(request,"about.html",context)

def  contact(request):
    context={}
    context['active_contact'] = "active"
    context['a'] ="line"
    if request.method=="POST":
        
        fname=request.POST['fname']
        lname=request.POST['lname']
        emal=request.POST['email']
        phn_num=request.POST['phn_num']
        msg=request.POST['msg']
        data=contactmodel(firstname=fname,lastname=lname,email=emal,phonenum=phn_num,message=msg)
        data.save()


        print(fname)
    return render(request,"contact.html",context)

# @login_required
def course(request):
    return render(request, "course.html")

def catagory(request):
    cats = courses_catagory.objects.all().order_by("-cat_name")
    for i in cats:
        print(i.cover_image)
    return render(request,"catagory.html",{"catagory":cats})



# for check user is present or not use in base.html 
def check_user(request):
    if request.method == "GET":
        un = request.GET["usern"]
        check = User.objects.filter(username=un)
        if len(check) == 1 :
            return HttpResponse("exist")
        else:    
        
            return HttpResponse("not exist")


def usr_login(request):
    if request.method=="POST":
        un = request.POST["username"]
        pwd = request.POST["pswd"]
        user = authenticate(username=un,password=pwd)
        if user:
            login(request,user)
            if user.is_superuser:
                return HttpResponseRedirect("/admin")
            if user.is_staff:
                return HttpResponseRedirect('/teacher_dashboard')
            if user.is_active:
                return HttpResponseRedirect("/teacher_dashboard")
        else:
            return render(request,"home.html",{"status":"Invalid Password or username"})
@login_required
def teacher_dashboard(request):
    context ={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    return render(request,"teacher_dashboard.html",context)
@login_required
def student_dashboard(request):
    context ={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    return render(request,"student_dashboard.html",context)

@login_required
def user_logout(request):
    logout(request)
    return HttpResponseRedirect("/")

def edit_profile(request):
    context ={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    if request.method == "POST":
        fn = request.POST['fname']
        ln = request.POST['lname']
        em = request.POST['email']
        age = request.POST['age']
        gen = request.POST['gen']
        about = request.POST['about']
        ct = request.POST['ct']
        occ = request.POST['occ']
        num  = request.POST['mob']
        
        usr = User.objects.get(id=request.user.id)
        usr.first_name = fn
        usr.last_name = ln
        usr.email = em
        usr.save()
        
        data.contact_number = num
        data.age = age
        
        data.gender = gen
        data.occupation = occ
        data.city= ct
        data.about = about
        data.save()
        if 'pic' in request.FILES:
            m=str(settings.MEDIA_ROOT)+"\\"
            os.remove(m+str(data.profile_pic))
            
            pic = request.FILES['pic']
            data.profile_pic = pic
            data.save()

        context["status"] = "Changes successfully"


    return render(request,"edit_profile.html",context)

def change_password(request):
    context ={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    if request.method=="POST":
        current = request.POST['cpwd']
        new = request.POST['npwd']
        user = User.objects.get(id=request.user.id)
        check = user.check_password(current)
        if check==True:
            user.set_password(new)
            context['msz'] ="password changed"
        else:
            context['msz'] ="Incorrect Current Password"
            
        
    return render(request,"change_password.html",context)
def add_courses(request):
    context ={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)
        context["data"] =data
    form = add_course_form()
    if request.method=="POST":
        form = add_course_form(request.POST,request.FILES)
    if form.is_valid():
        data=form.save(commit=False)
        login_user = User.objects.get(username=request.user.username)
        data.Teacher = login_user
        data.save()
        context["status"] = "{} Aded successfully".format(data.course_name)
        
    context['form'] = form
    return render(request,"add_course.html",context)

def my_course(request):
    context={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)
        context["data"] =data
    all = add_course.objects.filter(Teacher__id =request.user.id).order_by("-id")
    context['courses'] = all
    return render(request,"my_course.html",context)

def single_course(request):
    context={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    id = request.GET['cid']
    context['cid'] =id
    videos= add_video.objects.filter(video_catagory__id=id)
    context['video'] =videos
    obj= add_course.objects.get(id=id)
    # for payment 
    try:
        users=User.objects.get(username=request.user.username)
        pvideo = cart.objects.get(user=users,courses=obj,status=True)
        context['pvid'] = pvideo
        print(pvideo)
    except:
        pass    
    context['course'] = obj
    return render(request,"single_course.html",context)
def update_course(request):
    context={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    cats = courses_catagory.objects.all().order_by("cat_name")
    context["cats"] =cats
    cid = request.GET['cid']
    context['cid'] =cid
    course = add_course.objects.get(id=cid)
    context['course'] =course
    if request.method == "POST":
        
        cn = request.POST['cname']
        ccat_id = request.POST['ccat']
        cprice = request.POST['cprice']
        sprice= request.POST['sprice']
        detail = request.POST['detail']
        cat_obj = courses_catagory.objects.get(id = ccat_id)
        course.course_name=cn
        course.course_catagory = cat_obj
        course.course_price = cprice 
        course.Sale_price = sprice
        course.details = detail
        
        if 'cimg' in request.FILES:
            m=str(settings.MEDIA_ROOT)+"/"
            os.remove(m+str(course.course_image))
            img = request.FILES['cimg']
            course.course_image = img
        course.save()
        context['status '] = "changes done"
        context['id'] = cid




    return render(request,"update_course.html",context)
def delete_course(request):
    context={}
    if "cid"in request.GET:
        cid= request.GET["cid"]
        
        cours = get_object_or_404(add_course,id=cid)
        a= add_video.objects.filter(video_catagory__id=cid)
        
        context["cours"] = cours
        if "action" in request.GET:
            if (len(a)!=0):
                for i in a:
                    m=str(settings.MEDIA_ROOT)+"/"
                    os.remove(m+str(i.video))
                    os.remove(m+str(cours.course_image))
                    

                cours.delete()
            else:
                cours.delete()
                m=str(settings.MEDIA_ROOT)+"/"
                os.remove(m+str(cours.course_image))



            context['status']= str(cours.course_name)+"delete successfully"
    return render(request,"delete_course.html",context)
    

def all_courses(request):
    context={}
    context['a'] ="line"
    context['active_all_courses'] = "active"
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    all = add_course.objects.all().order_by("-id")
    context['courses'] = all
    if "cid" in request.GET:
        cid= request.GET['cid']
        cours= add_course.objects.filter(course_catagory__id=cid)
        context['courses'] = cours
    if request.method=="POST":
        q=request.POST['qry']
        cours= add_course.objects.filter(Q(course_name__icontains = q) | Q(details__icontains = q)) 
        context['courses'] =cours
        

    return render(request,"all_courses.html",context)    
def add_videos(request):
    context={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    check= request.user.username
    un=User.objects.get(username=check)
    cours= add_course.objects.filter(course_name=request.user)
    context["teacher"] = check
    all = add_course.objects.filter(Teacher__id =request.user.id).order_by("-id")
    context['courses'] = all
    if request.method=="POST":
        print(request.POST)
        vtitle= request.POST['vt']
        vdes = request.POST['vd']
        teacher = check
        cours_name = request.POST['cname']
        if request.FILES:
            print(request.FILES)
            video = request.FILES['video']
            cat=add_course.objects.get(course_name=cours_name)
            a=add_video(video_title=vtitle,video=video,Teacher=un,video_description=vdes,video_catagory=cat)
            a.save()
    print(check)

    return render(request,"add_video.html",context)
def videos_show(request):
    context={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    if "cid" in request.GET:
        cid= request.GET['cid']
        context['cid']=cid
        videos = add_video.objects.filter(video_catagory__id=cid)
        context['video'] =videos
    if "editvideo" in request.POST:
        a=request.POST['E']
        edit=add_video.objects.get(id=a)
        
        
        vt=request.POST['vname']
        vdisc=request.POST['vidisc']
        
        edit.video_title =vt
        edit.video_description =vdisc


        if "vupdate" in request.FILES:
            v=request.FILES['vupdate']
            edit.video=v

        edit.save()
    if "action" in request.GET:
        id=request.GET['vid']
        a = request.GET['cid']
        print(a)
        m=str(settings.MEDIA_ROOT)+"\\"
        delete_vid= get_object_or_404(add_video,id=id)
        os.remove(m+str(delete_vid.video))
        delete_vid.delete()
        
        return redirect('/videos_show/?cid={}'.format(a))
        context['delete'] ="video deleted successfully"
    return render(request,"videos_show.html",context)

def video_all(request):
    context={}

    return render(request,"course_video.html",context)

def forgot_pass(request):
    context ={}
    if request.method =="POST":
        un = request.POST["username"]
        npass = request.POST["npass"]
        user = get_object_or_404(User,username=un)
        user.set_password(npass)
        user.save()
        context["status"] ="Password changed successfully!!!!!!!!!!!!!"

        login(request,user)
        if user.is_superuser:
            return HttpResponseRedirect("/admin")
        else :
            return HttpResponseRedirect('/teacher_dashboard')




    return render(request,"forgot_password.html",context)

import random
def reset_pass(request):
    un = request.GET["Username"]
    try:
        user = get_object_or_404(User,username=un)
        otp = random.randint(1000,9999)
        msz ="Dear{} \n{} here is your one time password(otp)\n do not share with others".format(user.username,otp)
        try:
            email= EmailMessage("Account Verification",msz,to=[user.email])
            email.send()
            return JsonResponse({"status":"sent","email":user.email,"rotp":otp})

        except:
            return JsonResponse({"status":"error","email":user.email})
        
        
    except:
        return JsonResponse({"status": "failed"})



def process_payment(request):
    if request.method=="GET":
        order_id = request.GET.get('cid')
        print(order_id)
        order = get_object_or_404(add_course, id=order_id)
        print(order.Sale_price)
        amt=order.Sale_price
        inv = "INV-10000010455"+str(order.id)
        course=order.course_name

        

    # host = request.get_host()
 
    paypal_dict = {
        'business': settings.PAYPAL_RECEIVER_EMAIL,
        'amount': str(amt),
        'invoice': inv,
        'item_name': course,
        
        'currency_code': 'USD',
        'notify_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('paypal-ipn')),
        'return_url': 'http://{}{}'.format('127.0.0.1:8000',
                                           reverse('payment_done')),
        'cancel_return': 'http://{}{}'.format('127.0.0.1:8000',
                                              reverse('payment_cancelled')),
    }
    usr =  User.objects.get(username = request.user.username)
    print(usr)
        

    ord_obj = get_object_or_404(add_course,id=order_id)

    a=cart(user=usr,courses=ord_obj)
    a.save()
    request.session['User'] = usr.id
    request.session['course'] = ord_obj.id

    form = PayPalPaymentsForm(initial=paypal_dict)
    return render(request, 'process_payment.html',{ 'form': form})
def payment_done(request):
    if "User" in request.session:
        usr = request.session['User']
        course = request.session['course']
        
        us =  User.objects.get(id=usr )
        cs = get_object_or_404(add_course,id=course)
        ord_obj = cart.objects.get(user=us,courses=cs)
        ord_obj.status =True
        ord_obj.save()
        
        
        
        
    return render(request,"payment_success.html")
    # return render(request,'all_courses.html')
def payment_cancelled(request):
    
    return render(request,"paytment_failed.html")

def Enrolled_courses(request):
    context ={}
    check= register_user.objects.filter(user__id=request.user.id)
    if len(check)>0:
        data = register_user.objects.get(user__id=request.user.id)

        context["data"] =data
    us= User.objects.get(username=request.user.username)
    enc= cart.objects.filter(user=us,status=True)
    context['enc'] =enc
    
    return render(request,"Encrolled_courses.html",context)

def feedbacks(request):
    context={}
    if request.method=="POST":
        # fedbak = request.user.username
        # print(fedbak)
        f_sub = request.POST['fed_sub']
        f_detail= request.POST['fed_detail']
        a=feedback(feedacker=request.user.username,feed_sub=f_sub,feed_detail=f_detail)
        a.save()
        context['status'] ="Thankyou for Feedback "

    return render(request,"feedback.html",context)