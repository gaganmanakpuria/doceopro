from django.db import models
from django.contrib.auth.models import User
import datetime



# Create your models here.
class contactmodel(models.Model):
    firstname=models.CharField(max_length=250)
    lastname=models.CharField(max_length=250)
    email=models.EmailField(max_length=250)
    phonenum=models.ImageField(unique=True)
    message=models.TextField()
    
    def __str__(self):
        return self.firstname

class courses_catagory(models.Model):
    cat_name = models.CharField(max_length=250)
    cover_image = models.FileField(upload_to="media/%y/%m/%d")
    description = models.TextField()
    added_on = models.DateField(auto_now_add=True)
    
    def __str__(self):
        return self.cat_name

    class Meta:
        verbose_name_plural = "courses_catagory"



class register_user(models.Model):
    user = models.OneToOneField(User,on_delete= models.CASCADE)
    contact_number = models.IntegerField()
    profile_pic = models.ImageField(upload_to="profiles/%y/%m/%d" ,null=True, blank=True)
    age = models.CharField(max_length=250,null=True,blank=True )
    city = models.CharField(max_length=250,null=True)
    about = models.TextField(blank=True,null=True)
    gender = models.CharField(max_length=250,blank=True , null=True)
    occupation = models.CharField(max_length=250,blank=True,null=True)
    added_on = models.DateTimeField(auto_now_add=True,null=True)
    update_on = models.DateTimeField(auto_now=True,null=True)
    def __str__(self):
        return self.user.username

class add_course(models.Model):
    Teacher = models.ForeignKey(User,on_delete=models.CASCADE)
    course_name = models.CharField(max_length=250)
    course_catagory = models.ForeignKey(courses_catagory,on_delete=models.CASCADE)
    course_price = models.CharField(max_length=250)
    Sale_price  = models.CharField(max_length=250)
    course_image = models.FileField(upload_to="courses/%Y/%m/%d",null=True)
    details = models.TextField()
    

    def __str__(self):
        return self.course_name



class add_video(models.Model):
    Teacher =models.ForeignKey(User,on_delete=models.CASCADE,null=True)
    video_title = models.CharField(max_length=250)
    video_description = models.TextField()
    video_catagory = models.ForeignKey(add_course,on_delete=models.CASCADE)
    video = models.FileField(upload_to="videos/%y/%m/%d")

    def __str__(self):
        return self.video_title
class  cart(models.Model):
    user = models.ForeignKey(User,on_delete=models.CASCADE)
    courses = models.ForeignKey(add_course,on_delete=models.CASCADE)
    status= models.BooleanField(default=False)
    added_on = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
        return self.user.username
class feedback(models.Model):
    feedacker=models.CharField(max_length=250,null=True,blank=True)
    feed_sub=models.CharField(max_length=250)
    feed_detail = models.TextField()
    def __str__(self):
        return self.id
