from django.contrib import admin
from DOCEOAPP.models import contactmodel,courses_catagory,register_user,add_course,add_video,cart,feedback




# Register your models here.
admin.site.register(contactmodel)
admin.site.register(courses_catagory)
admin.site.register(register_user)
admin.site.register(add_course)
admin.site.register(add_video)
admin.site.register(cart)
admin.site.register(feedback)