from django import forms
from  DOCEOAPP.models import add_course

class add_course_form(forms.ModelForm):
    class Meta:
        model = add_course
        # fields ="__all__"
        exclude = ('Teacher',)

