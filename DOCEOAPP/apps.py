from django.apps import AppConfig


class DoceoappConfig(AppConfig):
    name = 'DOCEOAPP'
