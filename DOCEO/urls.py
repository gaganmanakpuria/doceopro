"""DOCEO URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.0/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path,include
from DOCEOAPP import views
from django.conf import settings
from django.conf.urls.static import static



urlpatterns = [
    path('admin/', admin.site.urls),
    path('',views.Home,name="home"),
    path('about/',views.about,name="about"),
    
    path('contact/',views.contact,name="contact"),
    path('course/',views.course,name="course"),
    path('catagory/',views.catagory,name="catagory"),
    path('check_user/',views.check_user,name="check_user"),
    path('user_login/',views.usr_login,name="user_login"),
    path('teacher_dashboard/',views.teacher_dashboard,name="teacher_dashboard"),
    path('student_dashboard/',views.student_dashboard,name="student_dashboard"),
    path('user_logout/',views.user_logout,name="user_logout"),
    path('edit_profile/',views.edit_profile,name="edit_profile"),
    path('change_password/',views.change_password,name="change_password"),
    path('add_course/',views.add_courses,name="add_course"),
    path('my_course/',views.my_course,name="my_course"),
    path('single_course/',views.single_course,name="single_course"),
    path('delete_course/',views.delete_course,name="delete_course"),
    path('update_course/',views.update_course,name="update_course"),
    path('all_courses/',views.all_courses,name="all_courses"),
    path('add_video/',views.add_videos,name="add_video"),
    path('videos_show/',views.videos_show,name="videos_show"),
    path('video_all/',views.video_all,name="video_all"),
    path("forgot_pass/",views.forgot_pass,name="forgot_pass"),
    path("reset_pass/",views.reset_pass,name="reset_pass"),
    # path('accounts/',include('allauth.urls')),
    path('process_payment/',views.process_payment,name="process_payment"),
    path('payment_done/',views.payment_done,name="payment_done"),
    path('payment_cancelled/',views.payment_cancelled,name="payment_cancelled"),
    path('feedback/',views.feedbacks,name="feedback"),
    path('Enrolled_courses/',views.Enrolled_courses,name="Enrolled_courses"),
    path('paypal/', include('paypal.standard.ipn.urls')),


]+static(settings.MEDIA_URL,document_root = settings.MEDIA_ROOT)
